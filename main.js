$(function(){
	$("#social-box").hide();

  $(".element").typed({
    strings: [
    " ^2000",
    "Yeah.^200.^200.^600",
    "I'm just kidding.^200.^200.^600", 
    "My name is Diego Jara ^600",
    "And this is Muggle Cat ^1000",
    "I'm currently working on this page ^600",
    "So, stay tuned ^1000",
    "Meanwhile.^200.^200.^600",
    "We can talk here: ^400"
    ],
    typeSpeed: 30,
    callback: ()=>{
    	$("#message-box").fadeOut("slow", ()=>{
    		$("#social-box").fadeIn("slow");
    	});
    	
    }
  });
});
